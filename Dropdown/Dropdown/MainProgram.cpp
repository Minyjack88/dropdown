#include "GL\glut.h"

int height = 600;
int width = 800;

void window(int height, int width)
{
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(90, width / height, 0, 1000);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

}
void GameLoop()
{
	glBegin(GL_TRIANGLES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3d(0.5f, 0.0f, 0.0f);
	glVertex3d(0.0f, 0.5f, 0.0f);
	glVertex3d(0.0f, 0.0f, 0.0f);
	glEnd();

}
void main(int &argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize(width, height);
	glutInitWindowPosition(0,0);

	glutCreateWindow("Dropdown");

	glutReshapeFunc(&window);
	glutDisplayFunc(&GameLoop);
	glutMainLoop();

}
